<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Piekarnia - wyniki przetwarzania zamówienia</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body class="container">
<h1>Piekarnia</h1>
<h2>Zamówienia klientów</h2>
<?php

$document_root = $_SERVER['DOCUMENT_ROOT'];

$wp = fopen("$document_root/gitlab/zapis do pliku/orders.txt", 'rb');
flock($wp,LOCK_SH);   //LOCK_SH blokada odczytu pliku, możliwość korzystania z pliku z innymi czytającymi

if (!$wp){
    echo "<p><strong>Brak zamówień.<br/>
        Proszę spróbować później.</strong></p>";
    exit;
}

while (!feof($wp)){
    $order = fgets($wp);
    echo htmlspecialchars($order)."<br/>";
}
flock($wp,LOCK_UN);  //zwolnienie blokady pliku
fclose($wp);

?>