<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Piekarnia - wyniki zamówienia</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body class="container">

<h1>Piekarnia</h1>
<h2>Wyniki zamówienia :</h2>
<?php

    //krótkie nazwy zmiennych
    $amoundOfBread = $_POST['amoundOfBread'] ?:0;
    $numberOfRolls = $_POST['numberOfRolls'] ?:0;
    $numberOfDonuts = $_POST['numberOfDonuts'] ?:0;
    $source = $_POST['source'];

    $quantity = $amoundOfBread + $numberOfRolls + $numberOfDonuts;

    $dateOfMakingOrder = '<p>Zamówienie przyjęte o '. date('H:i, jS F Y').'</p>';
    if ($dateOfMakingOrder < $quantity){
        echo $dateOfMakingOrder;
    }

    if ($numberOfRolls < 10 )
        $discount = 1;
    elseif ($numberOfRolls >= 10 && $numberOfRolls <= 49 )
        $discount = 0.95;
    elseif ($numberOfRolls >= 50 && $numberOfRolls <= 99)
        $discount = 0.9 ;
    elseif ($numberOfRolls >= 100)
        $discount = 0.85;

    if ($quantity==0){
        echo "Na poprzedniej stronie nie zostało złożone żadne zamówienie! <br/>";
        exit;
    }else
        echo "Zamówiono: </br>";
        {
        if ($amoundOfBread>0)
        echo htmlspecialchars($amoundOfBread).' chleba <br/>';
        if ($numberOfRolls>0)
        echo htmlspecialchars($numberOfRolls).' bułek <br/>';
        if ($numberOfDonuts>0)
        echo htmlspecialchars($numberOfDonuts).' pączków <br/><br/>';
    }
    echo "</br>";

    $value = 0.00;

    define('BREADPRICE',4);
    define ('PRICEOFROLLS',0.7);
    define('PRICEOFDONUTS',1.7);

    $value = $amoundOfBread * BREADPRICE
        + ($numberOfRolls * $discount) * PRICEOFROLLS
        + $amoundOfBread * PRICEOFDONUTS;

    echo "Cena netto: ".number_format($value,2)."PLN<br/>";

    $vatRate = 0.22; // stawka vat wynosi 22%
    $value = $value * (1 + $vatRate);
    echo "Cena brutto: ".number_format($value,2)."PLN</p>";

    echo "Źródło informacji:";
    switch ($source){
        case "a":
            echo "<p>Stały klient</p>";
            break;
        case "b":
            echo "<p>Reklama telewizyjna</p>";
            break;
        case "c":
            echo "<p>Książka telefoniczna</p>";
            break;
        case "d":
            echo "<p>Znajomy</p>";
            break;
        default:
            echo "<p>Źródło nieznane<p/>";
            break;
    }
    // Zapisywanie do pliku

    $address = $_POST['address'];
    $data=date('H:i, jS F Y');
    $document_root = $_SERVER['DOCUMENT_ROOT'];

    $outputString = $data."\t".$amoundOfBread." chleba \t".$numberOfRolls." bułek\t"
        .$numberOfDonuts." pączków\t".number_format($value,2)
        ."PLN\t". $address."\n";

    $wp = fopen("$document_root/gitlab/zapis do pliku/orders.txt",'ab');

    if (!$wp){
        echo "<p><strong>Zamówienie państwa nie może zostać przyjęte w tej chwili.
        Proszę spróbować później.</strong></p>";
        exit;
    }

    fwrite($wp,$outputString);
    fclose($wp);

?>
</body>
</html>